SUMMARY = "Development image include"

DEVELOPMENT_TOOLS = " \
                     coreutils \
                     devmem2 \
                     dropbear \
                     dtc \
                     gdbserver \
                     iproute2 \
                     iproute2-tc \
                     iw \
                     kexec-tools \
                     ldd \
                     memtester \
                     mtd-utils \
                     perf \
                     procps \
                     strace \
                     sysbench \
                     v4l-utils \
                     valgrind \
                     wpa-supplicant-cli \
                    "

IMAGE_INSTALL += " \
  ${DEVELOPMENT_TOOLS} \
"

DEBUG_BUILD = "1"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT= "1"

